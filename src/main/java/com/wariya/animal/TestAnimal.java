/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wariya.animal;

/**
 *
 * @author os
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();

        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        System.out.println("a1 is poultry animal ? " + (a1 instanceof Poultry));
        System.out.println("a1 is aquaticanimal animal ? " + (a1 instanceof AquaticAnimals));

        
        Dog d1 = new Dog("Black");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animal ? " + (d1 instanceof Animal));
        System.out.println("d1 is land animal ? " + (d1 instanceof LandAnimal));
        Animal a2 = d1;
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is reptile animal ? " + (a2 instanceof Reptile));
        System.out.println("a2 is poultry animal ? " + (a2 instanceof Poultry));
        System.out.println("a2 is aquaticanimal animal ? " + (a2 instanceof AquaticAnimals));
        
        Cat c1 = new Cat("Yuumi");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animal ? " + (c1 instanceof Animal));
        System.out.println("c1 is land animal ? " + (c1 instanceof LandAnimal));
        Animal a3 = c1;
        System.out.println("a3 is land animal ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is reptile animal ? " + (a3 instanceof Reptile));
        System.out.println("a3 is poultry animal ? " + (a3 instanceof Poultry));
        System.out.println("a3 is aquaticanimal animal ? " + (a3 instanceof AquaticAnimals));
        
        Crocodile cro1 = new Crocodile("Renekton");
        cro1.crawl();
        cro1.eat();
        cro1.sleep();
        cro1.speak();
        cro1.walk();
        System.out.println("cro1 is animal ? " + (cro1 instanceof Animal));
        System.out.println("cro1 is reptile ? " + (cro1 instanceof Reptile));
        Animal a4 = cro1;
        System.out.println("a4 is land animal ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is reptile animal ? " + (a4 instanceof Reptile));
        System.out.println("a4 is poultry animal ? " + (a4 instanceof Poultry));
        System.out.println("a4 is aquaticanimal animal ? " + (a4 instanceof AquaticAnimals));
        
        Snake s1 = new Snake("Viper");
        s1.crawl();
        s1.eat();
        s1.sleep();
        s1.speak();
        s1.walk();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        System.out.println("s1 is reptile ? " + (s1 instanceof Reptile));
        Animal a5 = s1;
        System.out.println("a5 is land animal ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is reptile animal ? " + (a5 instanceof Reptile));
        System.out.println("a5 is poultry animal ? " + (a5 instanceof Poultry));
        System.out.println("a5 is aquaticanimal animal ? " + (a5 instanceof AquaticAnimals));
        
        Crab crab1 = new Crab("Sebastian");
        crab1.sleep();
        crab1.eat();
        crab1.speak();
        crab1.swim();
        crab1.walk();
        System.out.println("crab1 is animal ? " + (crab1 instanceof Animal));
        System.out.println("crab1 is AquaticAnimal ? " + (crab1 instanceof AquaticAnimals));
        Animal a7 = crab1;
        System.out.println("a7 is land animal ? " + (a7 instanceof LandAnimal));
        System.out.println("a7 is reptile animal ? " + (a7 instanceof Reptile));
        System.out.println("a7 is poultry animal ? " + (a7 instanceof Poultry));
        System.out.println("a7 is aquaticanimal animal ? " + (a7 instanceof AquaticAnimals));
        
        Fish f1 = new Fish("Fizz");
        f1.eat();
        f1.sleep();
        f1.speak();
        f1.swim();
        f1.walk();
        System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        System.out.println("f1 is AquaticAnimal ? " + (f1 instanceof AquaticAnimals));
        Animal a6 = f1;
        System.out.println("a6 is land animal ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is reptile animal ? " + (a6 instanceof Reptile));
        System.out.println("a6 is poultry animal ? " + (a6 instanceof Poultry));
        System.out.println("a6 is aquaticanimal animal ? " + (a6 instanceof AquaticAnimals));
        
        bat b1 =new bat("Sparrow");
        b1.eat();
        b1.fly();
        b1.sleep();
        b1.speak();
        b1.walk();
        System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        System.out.println("b1 is Poultry ? " + (b1 instanceof Poultry));
        Animal a8 = b1;
        System.out.println("a8 is land animal ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is reptile animal ? " + (a8 instanceof Reptile));
        System.out.println("a8 is poultry animal ? " + (a8 instanceof Poultry));
        System.out.println("a8 is aquaticanimal animal ? " + (a8 instanceof AquaticAnimals));
        
        Bird bird1 =new Bird("Anivia");
        bird1.eat();
        bird1.fly();
        bird1.sleep();
        bird1.speak();
        bird1.walk();
        System.out.println("bird1 is animal ? " + (bird1 instanceof Animal));
        System.out.println("bird1 is Poultry ? " + (bird1 instanceof Poultry));
        Animal a9 = bird1;
        System.out.println("a9 is land animal ? " + (a9 instanceof LandAnimal));
        System.out.println("a9 is reptile animal ? " + (a9 instanceof Reptile));
        System.out.println("a9 is poultry animal ? " + (a9 instanceof Poultry));
        System.out.println("a9 is aquaticanimal animal ? " + (a9 instanceof AquaticAnimals));







    }
}


